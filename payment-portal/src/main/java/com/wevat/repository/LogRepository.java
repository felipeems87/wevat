package com.wevat.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.wevat.entity.PaymentLog;

/**
 * Contract for log repository implementations
 * 
 * @author felipesabino
 *
 */
public interface LogRepository extends CrudRepository<PaymentLog, Long> {

	Optional<List<PaymentLog>> findAll(Pageable pageable);
	
}
