package com.wevat.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import com.wevat.entity.User;

/**
 * Contract for user repository implementations
 * 
 * @author felipesabino
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {

	Optional<List<User>> findAll(Pageable pageable);
	
}
