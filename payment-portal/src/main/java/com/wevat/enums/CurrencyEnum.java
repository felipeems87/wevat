package com.wevat.enums;

import java.util.Arrays;
import java.util.Optional;

/**
 * Enum containing corrency code and description
 * @author felipesabino
 *
 */
public enum CurrencyEnum {
	
	EUR("EUR", "European euro"),
    USD("USD", "United States dollar"),
    GBP("GBP", "Pound sterling"),
    BRL("BRL", "Brazilian real");
	
	private String currencyCode;
	private String currencyDescription;

    /**
     * Constructor
     * 
     * @param currencyCode
     * @param currencyDescription
     */
    CurrencyEnum(String currencyCode, String currencyDescription) {
        this.currencyCode = currencyCode;
        this.currencyDescription = currencyDescription;
    }

    public String currencyCode() {
        return currencyCode;
    }

    public String currencyDescription() { 
        return currencyDescription;
    }
    
    /**
     * Check if the currency exists in the Enum
     * 
     * @param currency
     * @return
     */
    public static Optional<CurrencyEnum> validateCurrencyCode(String currency) {
    	return Arrays.stream(CurrencyEnum.values()).
		filter(enumValue -> enumValue.currencyCode().equals(currency)).findAny();
    }
}
