package com.wevat.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Contract for payment service implementations
 * 
 * @author felipesabino
 *
 */
public interface IPaymentController {

	/**
	 * Create a payment in the database
	 * 
	 * @param id
	 * @param currency
	 * @return
	 */
	public ResponseEntity<?> doPayment(@PathVariable String id, @PathVariable String currency);
	
	
	/**
	 * Return all logs from database considering pagination parameters
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	public ResponseEntity<?> getLogInfo(@PathVariable String page, @PathVariable String size);

}
