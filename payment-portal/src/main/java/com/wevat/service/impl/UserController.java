package com.wevat.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.wevat.constant.MessageConstant;
import com.wevat.entity.User;
import com.wevat.exception.InvalidParameterException;
import com.wevat.repository.UserRepository;
import com.wevat.service.IUserController;

/**
 * Class responsible for the user endpoints implementations.
 * 
 * @author felipesabino
 *
 */
@RestController
@RequestMapping("/user")
public class UserController extends PortalController implements IUserController {

	@Autowired
	private UserRepository userRepository;

	/* (non-Javadoc)
	 * @see com.wevat.service.IUserController#getUserById(java.lang.String)
	 */
	@RequestMapping(value = "/get-user/{id}", method = RequestMethod.GET)
	public ResponseEntity<Optional<User>> getUserById(String id) {
		
		if(StringUtils.isEmpty(id) || !StringUtils.isNumeric(id)) {
			throw new InvalidParameterException(MessageConstant.INVALID_USER_ID);
		}
		
		Long userId = Long.parseLong(id);
		
		Optional<User> user = userRepository.findById(userId);

		return ResponseEntity.ok().body(user);
	}
	
	/* (non-Javadoc)
	 * @see com.wevat.service.IUserController#getUsers(java.lang.String, java.lang.String)
	 */
	@RequestMapping(value = "/get-users/{page}/{size}", method = RequestMethod.GET)
	public ResponseEntity<Optional<List<User>>> getUsers(@PathVariable String page, @PathVariable String size) {
		
		if(StringUtils.isEmpty(page) || !StringUtils.isNumeric(page) || 
				StringUtils.isEmpty(size) || !StringUtils.isNumeric(size)) {
			throw new InvalidParameterException(MessageConstant.INVALID_PAGINATION_PARAMS);
		}
		
		int resultPage = Integer.parseInt(page);
		int resultSize = Integer.parseInt(size);
		
		Optional<List<User>> results = userRepository.findAll(PageRequest.of(resultPage, resultSize));
		
		return ResponseEntity.ok().body(results);
	}
}
