package com.wevat.service.impl;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Class responsible for handling errors in the parent controllers
 * 
 * @author felipesabino
 *
 */
public class PortalController implements ErrorController {
	private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {
        return "We are really sorry that we could not process your request.";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}
