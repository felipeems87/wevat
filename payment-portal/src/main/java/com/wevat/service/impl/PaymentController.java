package com.wevat.service.impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wevat.client.PartnerClient;
import com.wevat.constant.MessageConstant;
import com.wevat.constant.PartnerConstantsData;
import com.wevat.entity.PaymentLog;
import com.wevat.enums.CurrencyEnum;
import com.wevat.exception.InvalidParameterException;
import com.wevat.exception.PartnerPaymentException;
import com.wevat.exception.PaymentLogException;
import com.wevat.exception.UserNotFoundException;
import com.wevat.repository.LogRepository;
import com.wevat.repository.UserRepository;
import com.wevat.service.IPaymentController;

/**
 * Class responsible for the payment log endpoints implementations.
 * 
 * @author felipesabino
 *
 */
@RestController
@RequestMapping("/payment")
public class PaymentController extends PortalController implements IPaymentController{
    
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private LogRepository logRepository;
	
	/* (non-Javadoc)
	 * @see com.wevat.service.IPaymentController#doPayment(java.lang.String, java.lang.String)
	 */
	@RequestMapping(value = "/do-payment/{id}/{currency}", method = RequestMethod.GET)
	public ResponseEntity<Optional<PaymentLog>> doPayment(@PathVariable String id, @PathVariable String currency) {
		
		// validating parameters
		if(StringUtils.isEmpty(id) || !StringUtils.isNumeric(id)) {
			throw new InvalidParameterException(MessageConstant.INVALID_USER_ID);
		}

		if(!CurrencyEnum.validateCurrencyCode(currency).isPresent()) {
			throw new InvalidParameterException(MessageConstant.INVALID_CURRENCY_CODE);
		}
		
		Long userId = Long.parseLong(id);
		if(!userRepository.findById(userId).isPresent()) {
			throw new UserNotFoundException(MessageConstant.USER_NOT_FOUND);
		}

		Optional<String> result = new PartnerClient().payWithPartner(getPaymentInfoJsonParameter());
		if(!result.isPresent())
			throw new PartnerPaymentException(MessageConstant.PARTNER_PAYMENT_ERROR);
		
		PaymentLog log;
		try {
			log = logRepository.save(new ObjectMapper().readValue(result.get(), PaymentLog.class));
		} catch (Exception e) {
			e.printStackTrace();
			throw new PaymentLogException(MessageConstant.PAYMENT_LOG_ERROR);
		}
		
		return ResponseEntity.ok().body(Optional.of(log));
	}
	
	/* (non-Javadoc)
	 * @see com.wevat.service.IPaymentController#getLogInfo(java.lang.String, java.lang.String)
	 */
	@RequestMapping(value = "/get-log-info/{page}/{size}", method = RequestMethod.GET)
	public ResponseEntity<Optional<List<PaymentLog>>> getLogInfo(@PathVariable String page, @PathVariable String size) {
		
		if(StringUtils.isEmpty(page) || !StringUtils.isNumeric(page) || 
				StringUtils.isEmpty(size) || !StringUtils.isNumeric(size)) {
			throw new InvalidParameterException(MessageConstant.INVALID_PAGINATION_PARAMS);
		}
		
		int resultPage = Integer.parseInt(page);
		int resultSize = Integer.parseInt(size);
		
		Optional<List<PaymentLog>> results = logRepository.findAll(PageRequest.of(resultPage, resultSize));
		
		return ResponseEntity.ok().body(results);
	}

	/**
	 * Create a json object with all information needed for using TransferWise API.
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private JSONObject getPaymentInfoJsonParameter() {
		JSONObject params = new JSONObject();
		try {
			params.put(PartnerConstantsData.PARAM_PROFILE, PartnerConstantsData.PROFILE);
		    params.put(PartnerConstantsData.PARAM_SOURCE, CurrencyEnum.EUR.currencyCode());
		    params.put(PartnerConstantsData.PARAM_TARGET, CurrencyEnum.GBP.currencyCode());
		    params.put(PartnerConstantsData.PARAM_RATE_TYPE, PartnerConstantsData.RATE_TYPE);
		    params.put(PartnerConstantsData.PARAM_TARGET_AMOUNT, PartnerConstantsData.TARGET_AMOUNT);
		    params.put(PartnerConstantsData.PARAM_TYPE, PartnerConstantsData.TYPE);
		} catch (Exception ex) {}
		return params;
	}
}
