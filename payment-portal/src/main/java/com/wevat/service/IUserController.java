package com.wevat.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Contract for user service implementations
 * 
 * @author felipesabino
 *
 */
public interface IUserController {

	/**
	 * Return a specific user from the database
	 * 
	 * @param id
	 * @return
	 */
	public ResponseEntity<?> getUserById(@PathVariable String id);
	
	/**
	 * Return all users from database considering pagination parameters
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	public ResponseEntity<?> getUsers(@PathVariable String page, @PathVariable String size);
	
}
