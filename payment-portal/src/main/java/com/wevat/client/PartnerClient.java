package com.wevat.client;

import java.util.Optional;

import org.json.simple.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

/**
 * Class responsible for connecting in TransferWise API using client implementation.
 * 
 * @author felipesabino
 *
 */
public class PartnerClient {
	
	private final String URI = "https://api.sandbox.transferwise.tech/v1/quotes";
	private final String USER_TOKEN = "dd8f7bf8-3ae7-4be6-8781-ee38b3408e77";
	
	/**
	 * Connect to the TransferWise API
	 * 
	 * @param params
	 * @return
	 */
	public Optional<String> payWithPartner(JSONObject params) {
	    RestTemplate restTemplate = new RestTemplate();
	    HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.APPLICATION_JSON);
	    headers.setBearerAuth(USER_TOKEN);
	    ResponseEntity<String> response = null;
	    
	    try {
	    	response = restTemplate.exchange(URI, HttpMethod.POST, new HttpEntity<String>(params.toString(), headers), String.class);
	    } catch (Exception ex) {
	    	return Optional.empty();
	    }
	    
	    if (response.getStatusCodeValue() == 200) {
            return Optional.ofNullable(response.getBody());
        }
	    
		return Optional.empty();
	}
}