package com.wevat;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RestController;
import com.wevat.entity.User;
import com.wevat.enums.CurrencyEnum;
import com.wevat.repository.UserRepository;

/**
 * @author felipesabino
 *
 */
@RestController
@SpringBootApplication
public class PaymentPortalApplication {

	/**
	 * Start SpringBoot application
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(PaymentPortalApplication.class, args);
	}
	
	/**
	 * Add data to the repository
	 * @param repository
	 * @return
	 */
	@Bean
	public CommandLineRunner demo(UserRepository repository) {
		return (args) -> {
			// save a couple of users
			repository.save(new User("Alan", "Partridge", CurrencyEnum.GBP.currencyCode()));
			repository.save(new User("Ron", "Swanson", CurrencyEnum.USD.currencyCode()));
		};
	}
}
