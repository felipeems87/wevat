package com.wevat.exception;

/**
 * Exception for payment log error
 * 
 * @author felipesabino
 *
 */
public class PaymentLogException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PaymentLogException(String message) {
        super(message);
    }
}
