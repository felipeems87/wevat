package com.wevat.exception;

/**
 * Exception for partner payment error
 * 
 * @author felipesabino
 *
 */
public class PartnerPaymentException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PartnerPaymentException(String message) {
        super(message);
    }
}
