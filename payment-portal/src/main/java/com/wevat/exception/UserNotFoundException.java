package com.wevat.exception;

/**
 * Exception for user not found error
 * 
 * @author felipesabino
 *
 */
public class UserNotFoundException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String message) {
        super(message);
    }
}
