package com.wevat.exception;

/**
 * Exception for invalid parameter errors
 * 
 * @author felipesabino
 *
 */
public class InvalidParameterException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidParameterException(String message) {
        super(message);
    }
}
