package com.wevat.exception;

import java.util.List;
import java.util.UUID;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * Class responsible for handling all known exceptions 
 * 
 * @author felipesabino
 *
 */
@ControllerAdvice
@Slf4j
public class ExceptionHandlerAdvice {

    /**
     * @param t
     * @return
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<ErrorObject> erro(Throwable t) {
        log.error("Error during execution: ", t);
        log.error("Cause: {}", t.getCause());
        String errorCode = UUID.randomUUID().toString();
        log.error("Error code: {}", errorCode);
        return ResponseEntity.unprocessableEntity().body(new ErrorObject("Ops! Please get in contact to get more information about this error."));
    }

    /**
     * @param e
     * @return
     */
    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorObject> serviceError(UserNotFoundException e) {
        log.error("An exception was caught{} ", e);
        return ResponseEntity.badRequest().body(new ErrorObject(e.getMessage()));
    }

    /**
     * @param e
     * @return
     */
    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity<ErrorObject> serviceError(InvalidParameterException e) {
        log.error("An exception was caught{} ", e);
        return ResponseEntity.badRequest().body(new ErrorObject(e.getMessage()));
    }

    /**
     * @param e
     * @return
     */
    @ExceptionHandler(PartnerPaymentException.class)
    public ResponseEntity<ErrorObject> serviceError(PartnerPaymentException e) {
        log.error("An exception was caught{} ", e);
        return ResponseEntity.badRequest().body(new ErrorObject(e.getMessage()));
    }

    /**
     * @param e
     * @return
     */
    @ExceptionHandler(PaymentLogException.class)
    public ResponseEntity<ErrorObject> serviceError(PaymentLogException e) {
        log.error("An exception was caught{} ", e);
        return ResponseEntity.badRequest().body(new ErrorObject(e.getMessage()));
    }

    /**
     * Class responsible for encapsulating error message and/or list os errors.
     * 
     * @author felipesabino
     *
     */
    public class ErrorObject {
        String message;
        List<String> errors;

        ErrorObject(String mensagem) {
            this.message = mensagem;
        }

        ErrorObject(String message, List<String> errors) {
            this.message = message;
            this.errors = errors;
        }

        public String getMessage() {
            return message;
        }

        public List<String> getErrors() {
            return errors;
        }
    }
}
