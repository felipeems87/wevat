package com.wevat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Entity representing the user
 * @author felipesabino
 *
 */
@Entity
@Getter 
@Setter
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;
    private String payoutCurrency;

    /**
     * Constructor
     */
    protected User() {}

    /**
     * @param firstName
     * @param lastName
     * @param payoutCurrency
     */
    public User(String firstName, String lastName, String payoutCurrency) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.payoutCurrency = payoutCurrency;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "User[id=%d, firstName='%s', lastName='%s', payoutCurrency='%s']",
                id, firstName, lastName, payoutCurrency);
    }
}
