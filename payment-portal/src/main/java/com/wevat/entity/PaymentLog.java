package com.wevat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * Entity representing the payment log
 * 
 * @author felipesabino
 *
 */
@Entity
@Getter 
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentLog {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long logId;
	
	private Long id;
    private String source;
    private String target;
    private Double sourceAmount;
    private Double targetAmount;
    private Double rate;
    private Double fee;

    /**
     * Constructor
     */
    protected PaymentLog() {}
    
    /**
     * @param partnerResponseId
     * @param source
     * @param target
     * @param sourceAmount
     * @param targetAmount
     * @param rate
     * @param fee
     */
    public PaymentLog(Long partnerResponseId, String source, String target, Double sourceAmount, Double targetAmount, Double rate, Double fee) {

    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return String.format(
                "PaymentLog[logId=%d, id='%s', source='%s', target='%s', sourceAmount='%d', targetAmount='%d', rate='%d', fee='%d']",
                logId, id, source, target, sourceAmount, targetAmount, rate, fee);
    }
}
