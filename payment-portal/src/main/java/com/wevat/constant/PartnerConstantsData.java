package com.wevat.constant;

/**
 * Project data constants
 * 
 * @author felipesabino
 *
 */
public final class PartnerConstantsData {

	public static final String RATE_TYPE = "FIXED";
	public static final double TARGET_AMOUNT = 600;
	public static final String TYPE = "BALANCE_PAYOUT";
	public static final Long PROFILE = new Long(252);
	
	public static final String PARAM_PROFILE = "profile";
	public static final String PARAM_SOURCE = "source";
	public static final String PARAM_TARGET = "target";
	public static final String PARAM_RATE_TYPE = "rateType";
	public static final String PARAM_TARGET_AMOUNT = "targetAmount";
	public static final String PARAM_TYPE = "TYOE";
}