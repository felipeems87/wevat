package com.wevat.constant;

/**
 * Project message constants.
 * 
 * @author felipesabino
 *
 */
public class MessageConstant {

	public static final String INVALID_USER_ID = "Invalid user identifier.";
	public static final String USER_NOT_FOUND = "Could not find user.";
	public static final String INVALID_CURRENCY_CODE = "Invalid currency.";
	public static final String PARTNER_PAYMENT_ERROR = "Could not register the payment with partner.";
	public static final String PAYMENT_LOG_ERROR = "Could not register the payment log.";
	public static final String PAYMENT_SUCCESS = "Payment generated with success.";
	public static final String INVALID_PAGINATION_PARAMS = "Invalid pagination parameters.";
}
