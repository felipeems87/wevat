package com.wevat.client;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import java.util.Optional;
import org.json.simple.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.wevat.constant.PartnerConstantsData;
import com.wevat.enums.CurrencyEnum;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PartnerClientTest {

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void test_ShouldReturnOk_payWithPartner() {
		Optional<String> result = new PartnerClient().payWithPartner(getPaymentInfoJsonParameter());
		assertNotNull(result.get());
	}
	
	@Test
	public void test_ShouldReturnEmpty_payWithPartner() {
		Optional<String> result = new PartnerClient().payWithPartner(getPaymentInvalidInfoJsonParameter());
		assertFalse(result.isPresent());
	}
	
	@SuppressWarnings("unchecked")
	private JSONObject getPaymentInvalidInfoJsonParameter() {
		JSONObject params = new JSONObject();
		try {
			params.put(PartnerConstantsData.PARAM_PROFILE, PartnerConstantsData.PROFILE);
		    params.put(PartnerConstantsData.PARAM_SOURCE, "");
		    params.put(PartnerConstantsData.PARAM_TARGET, "");
		    params.put(PartnerConstantsData.PARAM_RATE_TYPE, "");
		    params.put(PartnerConstantsData.PARAM_TARGET_AMOUNT, "");
		    params.put(PartnerConstantsData.PARAM_TYPE, PartnerConstantsData.TYPE);
		} catch (Exception ex) {}
		return params;
	}
	
	@SuppressWarnings({ "unchecked" })
	private JSONObject getPaymentInfoJsonParameter() {
		JSONObject params = new JSONObject();
		try {
			params.put(PartnerConstantsData.PARAM_PROFILE, PartnerConstantsData.PROFILE);
		    params.put(PartnerConstantsData.PARAM_SOURCE, CurrencyEnum.EUR.currencyCode());
		    params.put(PartnerConstantsData.PARAM_TARGET, CurrencyEnum.GBP.currencyCode());
		    params.put(PartnerConstantsData.PARAM_RATE_TYPE, PartnerConstantsData.RATE_TYPE);
		    params.put(PartnerConstantsData.PARAM_TARGET_AMOUNT, PartnerConstantsData.TARGET_AMOUNT);
		    params.put(PartnerConstantsData.PARAM_TYPE, PartnerConstantsData.TYPE);
		} catch (Exception ex) {}
		return params;
	}
}