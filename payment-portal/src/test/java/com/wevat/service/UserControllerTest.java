package com.wevat.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.wevat.entity.User;
import com.wevat.exception.InvalidParameterException;
import com.wevat.service.impl.UserController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserControllerTest {

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
    @Autowired
    private UserController userController;

    @Test
    public void contexLoads() throws Exception {
        assertThat(userController).isNotNull();
    }
    
	@Test
	public void test_ShouldReturnOk_getUser() {
		
		ResponseEntity<Optional<User>> res = userController.getUserById("1");
		
		assertEquals(HttpStatus.OK, res.getStatusCode());

		User userTest = new User(new Long(1), "Alan", "Partridge", "GBP");
		Optional<User> user = res.getBody();
		
		assertTrue(user.isPresent());
		assertThat(userTest).isEqualToComparingFieldByField(user.get());
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestNullId_getUser() {
		userController.getUserById(null);
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestEmptyId_getUser() {
		userController.getUserById("");
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestCharId_getUser() {
		userController.getUserById("a");
	}
    
	@Test
	public void test_ShouldReturnOk_getUsers() {
		
		ResponseEntity<Optional<List<User>>> res = userController.getUsers("0","2");
		
		assertEquals(HttpStatus.OK, res.getStatusCode());

		Optional<List<User>> users = res.getBody();
		assertTrue(users.isPresent());
		assertTrue(users.get().size() == 1);
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestNullPageParam_getUsers() {
		userController.getUsers(null,"2");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestNullSizeParam_getUsers() {
		userController.getUsers("1",null);
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestEmptyPage_getUsers() {
		userController.getUsers("","1");
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestEmptySize_getUsers() {
		userController.getUsers("0","");
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestCharPageParam_getUsers() {
		userController.getUsers("a","2");
	}

    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestCharSizeParam_getUsers() {
		userController.getUsers("1","a");
	}

}