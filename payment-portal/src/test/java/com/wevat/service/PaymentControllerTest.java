package com.wevat.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.wevat.entity.PaymentLog;
import com.wevat.exception.InvalidParameterException;
import com.wevat.exception.UserNotFoundException;
import com.wevat.service.impl.PaymentController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentControllerTest {

	@Rule
    public ExpectedException thrown = ExpectedException.none();
	
    @Autowired
    private PaymentController paymentController;

    @Test
    public void contexLoads() throws Exception {
        assertThat(paymentController).isNotNull();
    }
    
	@Test
	public void test_ShouldReturnOk_doPayment() {
		ResponseEntity<Optional<PaymentLog>> res = paymentController.doPayment("1", "GBP");
		
		assertEquals(HttpStatus.OK, res.getStatusCode());
		
		Optional<PaymentLog> pl = res.getBody();
		
		assertTrue(pl.isPresent());
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestEmptyId_doPayment() {
		paymentController.doPayment("", "GBP");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestNullId_doPayment() {
		paymentController.doPayment(null, "GBP");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestInvalidId_doPayment() {
		paymentController.doPayment("a", "GBP");
	}
    
	@Test(expected = UserNotFoundException.class)
	public void test_ShouldReturnBadRequestIdDontExist_doPayment() {
		paymentController.doPayment("1000", "GBP");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestInvalidCurrency_doPayment() {
		paymentController.doPayment("1", "GBsP");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestEmptyCurrency_doPayment() {
		paymentController.doPayment("1", "");
	}
    
	@Test(expected = InvalidParameterException.class)
	public void test_ShouldReturnBadRequestNullCurrency_doPayment() {
		paymentController.doPayment("1", null);
	}
}