# Wevat project challenge


## Technologies

* Java 8
* SpringBoot
* Maven
* JUnit

## Instructions

1. Clone the repository
2. Import maven projects in your IDE
3. Install maven dependencies: mvn install
4. Run project Gateway as Java Application
5. Run projects PaymentPortal and PaymentIntegration as Java Application

## Example for testing endpoints

### http://localhost:8080/payment-portal/payment/do-payment/1/GBP
### http://localhost:8080/payment-portal/user/get-user/1


## Next steps

1. Move partner client code to Payment-Integration project
2. Implement communication between Payment-Portal and Payment-Integration
3. Create a standalone project to handle common code. (Generate a jar with all entities and add this jar as dependency of the projects)