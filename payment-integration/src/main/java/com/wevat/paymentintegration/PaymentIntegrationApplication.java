package com.wevat.paymentintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class PaymentIntegrationApplication {

	@RequestMapping(value = "/pay-with-transferwise")
	public String payWithTransferWise() {
		return "Pay with TransferWise";
	}
	
	public static void main(String[] args) {
		SpringApplication.run(PaymentIntegrationApplication.class, args);
	}
}
